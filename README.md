
---

싸이렌오더 고개센터 입니다. 

사이렌오더에 대한 소개를 포함하고 있습니다.

---
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
** 목차 **  

* [고객센터 소개](index.html)
* [위치사용](guide/1.html)
* [전체메뉴](guide/2.html)
* [주문하기](guide/3.html)
* [결재하기](guide/4.html)
* [주문승인](guide/5.html)
* [히스토리](guide/6.html)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI 를 활용 합니다.

GitLab CI를 활용한 정정 페이지 생성 고객센터 입니다.
[`.gitlab-ci.yml`](.gitlab-ci.yml) 로 CI 잡이 정리 되어 있습니다.

```yaml
image: node:8.9

cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch latest # fetch latest stable version
  - gitbook install # add any requested plugins in book.json

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
  only:
    - master # this job will affect only the 'master' branch
```
